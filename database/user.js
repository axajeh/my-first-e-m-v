db.createCollection("users", {
  validator: {
    $jsonSchema: {
      bsonType: "object",
      required: ['userName', "firstName", "lastName", "password"],
      properties: {
        userName: {
          bsonType: 'string',
          description: 'userName must be string'
        },
        firstName: {
          bsonType: 'string',
          description: 'firstName must be string'
        },
        lastName: {
          bsonType: 'string',
          description: 'lastName must be string'
        },
        password: {
          bsonType: 'string',
          description: 'password must be string'
        },
        active: {
          bsonType: 'bool',
          description: 'active must be boolean'
        },
      }
    }
  }
})