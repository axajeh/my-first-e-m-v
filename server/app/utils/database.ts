import mongodb, { Db, MongoClient } from "mongodb";

class Database {
  private _client: MongoClient
  private _db: Db
  private _url = "mongodb+srv://amirxajeh:amirxajeh@cluster0.3mvgi.mongodb.net/shopV2?retryWrites=true&w=majority"

  /**
   * get db
   */
  public get db(): Db {
    if (this._db) return this._db
    return null
  }

  /**
   * connect
   */
  public async connect(): Promise<MongoClient | Error> {
    try {
      const client = await mongodb.MongoClient.connect(this._url, { useUnifiedTopology: true })
      this._client = client
      this._db = client.db()
      return client
    } catch (error) {
      console.log(error)
      return null
    }
  }


}

export default new Database()