import express, { Application, Request, Response, NextFunction, ErrorRequestHandler } from 'express'
import bodyParser from "body-parser";
import cors from 'cors'
// routes
import routes from "./routes";
// databse
import Database from "./utils/database";

const app: Application = express()

class ApplicationSetup {
  constructor() {
    this.setupCors()
    this.setupBodyParser()
    this.setupRoutes()
    this.setupErrorHandler()
    this.setupServer()
  }

  async setupServer() {
    try {
      const client = await Database.connect()
      if (client) {
        app.listen(3000, () => {
          console.log('connected to localhost:3000')
        })
      }
    } catch (error) {
      console.log(error)
    }

  }

  setupRoutes() {
    app.use('/api', routes)
  }

  setupErrorHandler() {
    app.use((error: ErrorRequestHandler, req: Request, res: Response, next: NextFunction) => {
      console.log('error', error)
    })
  }

  setupBodyParser() {
    app.use(bodyParser.json())
    app.use(bodyParser.urlencoded({ extended: true }))
  }

  setupCors() {
    app.use(cors({
      origin: "http://localhost:8080",
      optonsSuccessStatus: 200,
      methods: "GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS",
      credentials: true
    }))
  }
}

export default ApplicationSetup