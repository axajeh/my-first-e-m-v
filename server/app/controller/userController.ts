import { Request, Response, NextFunction } from 'express'

import User from './../model/user'

class UserController {
  async insert(req: Request, res: Response, next: NextFunction) {
    const userData: Schema.User = req.body.data
    const user = new User(userData)
    const result = await user.save()
    if (result.insertedCount == 1) {
      res.status(201).json({ status: 'success', insertedId: result.insertedId })
    } else {
      res.status(500).json({ status: 'failed' })
    }
  }
}

export default new UserController()