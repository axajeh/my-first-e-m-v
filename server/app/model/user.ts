
import Model from './model'

class UserModel extends Model {

  user: Schema.User

  constructor(user: Schema.User) {
    super()
    this.user = user
  }

  async save() {
    try {
      const result = await this.db.collection('users').insertOne(this.user)
      return result
    } catch (error) {
      return null
    }
  }
}

export default UserModel