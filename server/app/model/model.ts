import autoBind from 'auto-bind'

import DatabaseHelper from './../utils/database'

class Model {
  db = DatabaseHelper.db

  constructor() {
    autoBind(this)
  }
}

export default Model