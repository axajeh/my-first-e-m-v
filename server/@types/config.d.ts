declare global {
  namespace NodeJS {
    interface Global {
      config: {
        database: {
          url: string
        }
      }
    }
  }
}

export { }