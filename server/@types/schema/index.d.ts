declare global {
  namespace Schema {
    interface User {
      userName: string
      firstName: string
      lastName: string
      password: string
      active: boolean
      role: string
    }
  }
}

export { }